#!/usr/bin/python
#  -*- coding: utf8 -*-
from flask import Flask, request
from api.v1 import v1
from configuration import config as cnf


app = Flask(__name__)
app.register_blueprint(v1)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=cnf.PORT, threaded=True)
