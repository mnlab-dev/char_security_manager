from helpers.CommonHelper import getValIfKeyExists
import requests
from configuration import config as cnf

class AlertNotificationController:

    def handle_alert_notification(self, alert_notification_dict):
        vno_id = alert_notification_dict["user_id"]
        parameter_list = []
        for metric in alert_notification_dict["alert_metadata_list"]:
            if metric["metric_name"] == "DDOS_Attack":
                print(metric["parameter_list"])
                parameter_list = metric["parameter_list"]
        fw_ip = "10.100.80.59"
        abstract_flow_list = self.parameter_list_2_abstract_flow_list(parameter_list)
        print(abstract_flow_list)
        requests.post("http://{}:{}/createFlows/v2".format(fw_ip, cnf.FW_PORT), json=abstract_flow_list)

    def handle_policy_action(self, alert_notification_dict):
        ns_id = alert_notification_dict["PolicyAction"]["VNFD"]["ns_id"]
        vno_id = alert_notification_dict["PolicyAction"]["VNFD"]["user_id"]
        print(vno_id, ns_id)
        fw_ip = self.get_fw_ip(vno_id, ns_id)

        parameter_list = getValIfKeyExists(
            alert_notification_dict["PolicyAction"]["VNFD"]["script_data"],
            "parameter_list"
        )

        abstract_flow_list = self.parameter_list_2_abstract_flow_list(parameter_list)
        requests.post("http://{}:{}/createFlows/v2".format(fw_ip, cnf.FW_PORT), json=abstract_flow_list)


    # TODO: get FW IP from TeNOR API
    def get_fw_ip(self, vno_id, network_service_id):
        # ip = requests.get("http://10.30.0.227:8002/spm/ip/firewall?vno_id={}&nsd_id={}".format(vno_id,
        #                                                                                        network_service_id))
        # print(ip["ip"])
        ip = "10.100.80.59"
        return ip


    def parameter_list_2_abstract_flow_list(self, parameter_list):

        temp_protocol = None
        temp_ip_dst = None
        temp_port_dst = None
        abstract_flow_list = []
        for label in parameter_list:
            (label_key, label_val) = (label["label"]["key"], label["label"]["value"])
            if label_key == "protocol":
                temp_protocol = label_val
            elif label_key == "ip_dst":
                temp_ip_dst = label_val
            elif label_key == "port_dst":
                temp_port_dst = label_val

        if temp_protocol is not None and temp_ip_dst is not None and temp_port_dst is not None:
            for label in parameter_list:
                (label_key, label_val) = (label["label"]["key"], label["label"]["value"])
                if label_key.startswith("ip_src_"):
                    temp_abstract_flow_dict = {
                        "actions": "drop",
                        "ip_dst": temp_ip_dst,
                        "ip_src": label_val,
                        "port_dst": temp_port_dst,
                        "priority": 100,
                        "protocol": temp_protocol
                    }
                    abstract_flow_list.append(temp_abstract_flow_dict)
        return abstract_flow_list