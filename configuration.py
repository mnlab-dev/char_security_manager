import os
import configparser
import getpass


class config:

    # --------------------------  Global Variables  -----------------------------
    # Base Directory
    BASE_DIR = os.path.dirname(__file__)
    BASE_USER_DIR = os.path.join("/home", getpass.getuser())

    # Configuration File Directory
    CONF_FILE_DIR = os.path.join(BASE_DIR, "main.conf")

    config = configparser.ConfigParser()
    config.read(CONF_FILE_DIR)


    # -------------------------- Default --------------------------------------------

    # Server Port
    PORT = int(config['DEFAULT']['port'])

    # -------------------------- TeNOR -------------------------------------------

    TENOR_IP = config['TENOR']['ip']
    TENOR_PORT = config['TENOR']['port']

    # -------------------------- Firewall -------------------------------------------

    # FW_IP = config['TENOR']['ip']
    FW_PORT = config['FIREWALL']['port']
