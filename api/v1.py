from flask import Blueprint, request, jsonify
from controllers.AlertNotificationController import AlertNotificationController

v1 = Blueprint('v1', __name__, template_folder='templates')

# This receives the SPM policy action
@v1.route('/policyActionReceiver/v1', methods=['POST'])
def policy_action_receiver():
    alert_notification_controller = AlertNotificationController()
    json_content = request.json
    print(json_content)
    alert_notification_controller.handle_policy_action(json_content)
    # return jsonify(response)
    return "Action received!"


@v1.route('/alertNotificationReceiver/v1', methods=['POST'])
def alert_notification_receiver():
    alert_notification_controller = AlertNotificationController()
    json_content = request.json
    print(json_content)
    alert_notification_controller.handle_alert_notification(json_content)
    # return jsonify(response)
    return "Notification received!"
