from helpers.CommonHelper import getValIfKeyExists


class AbstractFlow():

    def __init__(self, id=None, ip_src=None, ip_dst=None, priority=None, protocol=None, port_src=None, port_dst=None,
                 rate_limit=None, actions=None):
        self.id = id
        self.ip_src = ip_src
        self.ip_dst = ip_dst
        self.priority = priority
        self.protocol = protocol
        self.port_src = port_src
        self.port_dst = port_dst
        self.rate_limit = rate_limit
        self.actions = actions

    def __repr__(self):
        return "<AbstractFlow(id='%s', ip_src='%s', ip_dst='%s', priority='%s', protocol='%s', port_src='%s', " \
               "port_dst='%s' rate_limit='%s' actions='%s')>" % \
               (self.id, self.ip_src, self.ip_dst, self.priority, self.protocol, self.port_src, self.port_dst,
                self.rate_limit, self.actions)

    def set(self, dict_representation):
        self.id = getValIfKeyExists(dict_representation, "id")
        self.ip_src = getValIfKeyExists(dict_representation, "ip_src")
        self.ip_dst = getValIfKeyExists(dict_representation, "ip_dst")
        self.priority = getValIfKeyExists(dict_representation, "priority")
        self.protocol = getValIfKeyExists(dict_representation, "protocol").lower()
        self.port_src = getValIfKeyExists(dict_representation, "port_src")
        self.port_dst = getValIfKeyExists(dict_representation, "port_dst")
        self.rate_limit = getValIfKeyExists(dict_representation, "rate_limit")
        self.actions = getValIfKeyExists(dict_representation, "actions")


    @property
    def serialize(self):
        serialized_obj = {
                "id": self.id,
                "ip_src": self.ip_src,
                "ip_dst": self.ip_dst,
                "priority": self.priority,
                "protocol": self.protocol,
                "port_src": self.port_src,
                "port_dst": self.port_dst,
                "rate_limit": self.rate_limit,
                "actions": self.actions
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "abstract_flow": self.serialize
        }
        return serialized_obj
